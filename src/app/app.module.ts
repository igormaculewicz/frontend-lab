import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GalleriesComponent} from './galleries/galleries.component';
import {GalleryComponent} from './galleries/gallery/gallery.component';
import {FooterComponent} from './footer/footer.component';

// https://github.com/jelgblad/angular2-masonry
import {NgxMasonryModule} from 'ngx-masonry';

// https://www.npmjs.com/package/ngx-perfect-scrollbar
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {MASONRY_OPTIONS, MASONRY_OPTIONS_TOKEN} from './config/masonry.config';
import {
  ENDPOINTS_CONFIG,
  ENDPOINTS_CONFIG_TOKEN,
  UNAUTHORIZED_ENDPOINTS,
  UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN
} from './config/endpoints.config';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NewsComponent} from './news/news.component';
import {LoginComponent} from './login/login.component';
import {AUTHORIZATION_CONSTANTS, AUTHORIZATION_CONSTANTS_TOKEN} from './constants/authorization.constant';
import {LogoutComponent} from './logout/logout.component';
import {SessionExpiredComponent} from './session-expired/session-expired.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthInterceptor} from './auth.interceptor';
import { UserManagementComponent } from './user/user-management/user-management.component';
import {MatTabsModule} from '@angular/material/tabs';
import { CountdownModule } from 'ngx-countdown';
import { PasswordChangeComponent } from './user/password-change/password-change.component';
import { UserResourcesComponent } from './user/user-resources/user-resources.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { BackButtonDirective } from './back-button.directive';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatMenuModule} from '@angular/material/menu';
import { UserSearchComponent } from './user/user-search/user-search.component';
import { MenuComponent } from './menu/menu.component';
import { NewsViewComponent } from './news/view/view.component';
import { NewsEditComponent } from './news/edit/edit.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { NewsDeleteComponent } from './news/delete/delete.component';
import { ScrollableComponent } from './scrollable/scrollable.component';
import { GalleryItemComponent } from './galleries/gallery/gallery-item/gallery-item.component';
import { GalleryAddComponent } from './galleries/gallery/add/gallery-add.component';
import { GalleryDetailsComponent } from './galleries/gallery/details/gallery-details.component';
import { ImageItemAddComponent } from './galleries/gallery/gallery-item/add/image-item-add.component';

@NgModule({
  declarations: [
    AppComponent,
    GalleriesComponent,
    GalleryComponent,
    FooterComponent,
    NewsComponent,
    LoginComponent,
    LogoutComponent,
    SessionExpiredComponent,
    UserManagementComponent,
    PasswordChangeComponent,
    UserResourcesComponent,
    UserProfileComponent,
    BackButtonDirective,
    UserSearchComponent,
    MenuComponent,
    NewsViewComponent,
    NewsEditComponent,
    NewsDeleteComponent,
    ScrollableComponent,
    GalleryItemComponent,
    GalleryAddComponent,
    GalleryDetailsComponent,
    ImageItemAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxMasonryModule,
    PerfectScrollbarModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    CountdownModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    CKEditorModule
  ],
  providers: [
    {provide: MASONRY_OPTIONS_TOKEN, useValue: MASONRY_OPTIONS},
    {provide: ENDPOINTS_CONFIG_TOKEN, useValue: ENDPOINTS_CONFIG},
    {provide: UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN, useValue: UNAUTHORIZED_ENDPOINTS},
    {provide: AUTHORIZATION_CONSTANTS_TOKEN, useValue: AUTHORIZATION_CONSTANTS},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
