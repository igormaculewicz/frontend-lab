import {Component, HostListener, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {ServerCommunicationService} from '../services/server-communication.service';
import {NgxMasonryOptions} from 'ngx-masonry';
import {MASONRY_OPTIONS_TOKEN} from '../config/masonry.config';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN} from '../config/endpoints.config';
import {GalleryInterface} from '../interfaces/gallery/gallery.interface';
import {PaginatedDataSource} from '../interfaces/paging/paginated-data-source.class';

@Component({
  selector: 'app-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.scss']
})
export class GalleriesComponent implements OnInit {
  galleries: PaginatedDataSource<GalleryInterface>;

  constructor(
    @Inject(MASONRY_OPTIONS_TOKEN) public masonryConfig: NgxMasonryOptions,
    @Inject(ENDPOINTS_CONFIG_TOKEN) public endpointsConfig: Endpoints,
    private serverCommunicationService: ServerCommunicationService
  ) {
  }

  ngOnInit() {
    this.galleries = new PaginatedDataSource<GalleryInterface>(
      request => this.serverCommunicationService.getGalleries(request),
      {property: 'name', direction: 'desc'}, 9
    );
  }


}
