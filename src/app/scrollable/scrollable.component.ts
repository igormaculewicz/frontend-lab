import {AfterViewInit, Component, HostListener, Input, OnInit, Output} from '@angular/core';
import {PaginatedDataSource} from '../interfaces/paging/paginated-data-source.class';
import {Observable} from 'rxjs';
import {Page, PageRequest} from '../interfaces/paging/page';

export type PaginatedEndpoint<T> = (req: PageRequest<T>) => Observable<Page<T>>

@Component({
  selector: 'app-scrollable',
  templateUrl: './scrollable.component.html',
  styleUrls: ['./scrollable.component.scss']
})
export class ScrollableComponent<T> implements OnInit, AfterViewInit {
  @Input() paginatedDataSource: PaginatedDataSource<T>;
  @Output() dataSource: T[];

  ngOnInit(): void {
    this.paginatedDataSource.page$.subscribe(page => {
      if (!this.dataSource) {
        this.dataSource = page.content;
      } else {
        this.dataSource = this.dataSource.concat(page.content);
      }
    });
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if (this.shouldLoad()) {
      this.paginatedDataSource.fetch(this.paginatedDataSource.currentPage.page + 1);
    }
  }

  ngAfterViewInit(): void {
    const x = setInterval(() => {
      this.onScroll();

      if (!this.shouldLoad()) {
        clearInterval(x);
      }
    }, 1000);
  }

  shouldLoad(): boolean {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      if (this.paginatedDataSource.currentPage.size * (this.paginatedDataSource.currentPage.page + 1) < this.paginatedDataSource.total) {
        return true;
      }
    }

    return false;
  }
}
