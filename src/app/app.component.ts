import {AfterViewInit, Component, OnInit} from '@angular/core';
import {GlobalActionsService} from './services/global-actions.service';
import {AuthorizationService} from './services/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'zpsb-angular-gallery';

  constructor(
    private globalActionsService: GlobalActionsService
  ) {

  }

  ngOnInit() {
    this.globalActionsService.onInit();
  }

  ngAfterViewInit(): void {
    this.globalActionsService.afterViewInit();
  }
}
