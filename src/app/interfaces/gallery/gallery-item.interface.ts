
export interface GalleryItemInterface {
  id: number;
  name: string;
  description: string;
  path: string;
  createdAt: string;
}
