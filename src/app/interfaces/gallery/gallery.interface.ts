import {CreatorInterface} from '../user/creator.interface';

export interface GalleryInterface {
  id: number;
  name: string;
  description: string;
  thumbnail: string;
  creator: CreatorInterface;
  privateGallery: boolean;
  createdAt: string;
}
