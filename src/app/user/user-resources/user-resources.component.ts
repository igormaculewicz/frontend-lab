import {Component, Inject, OnInit} from '@angular/core';
import {MASONRY_OPTIONS_TOKEN} from '../../config/masonry.config';
import {NgxMasonryOptions} from 'ngx-masonry';
import {PaginatedDataSource} from '../../interfaces/paging/paginated-data-source.class';
import {NewsItemInterface} from '../../interfaces/news/news-item.interface';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {GalleryInterface} from '../../interfaces/gallery/gallery.interface';

@Component({
  selector: 'app-user-resources',
  templateUrl: './user-resources.component.html',
  styleUrls: ['./user-resources.component.scss']
})
export class UserResourcesComponent implements OnInit {
  // TODO should be divided to independent componnets
  newsDisplayedColumns = ['title', 'createdAt', 'visible', 'options'];
  galleriesDisplayedColumns = ['name', 'createdAt', 'privateGallery', 'options'];

  newsData: PaginatedDataSource<NewsItemInterface>;
  galleriesData: PaginatedDataSource<GalleryInterface>;

  constructor(
    private serverCommunicationService: ServerCommunicationService
  ) {

  }

  ngOnInit(): void {
    this.newsData = new PaginatedDataSource<NewsItemInterface>(
      request => this.serverCommunicationService.getCurrentUserNews(request),
      {property: 'title', direction: 'desc'}, 5
    );

    this.galleriesData = new PaginatedDataSource<GalleryInterface>(
      request => this.serverCommunicationService.getCurrentUserGalleries(request),
      {property: 'name', direction: 'desc'}, 5
    );
  }

}
