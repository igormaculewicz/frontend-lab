export enum Role {
  ROLE_USER = 'ROLE_USER',
  ROLE_ADMIN = 'ROLE_ADMIN'
}

export interface TokenInfoInterface {
  exp: number;
  iat: number;
  iss: string;
  aud: string;
  jti: string;
  authorities: Role[];
}
