import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN} from '../config/endpoints.config';
import {NewsItemInterface} from '../interfaces/news/news-item.interface';
import {Page, PageRequest} from '../interfaces/paging/page';
import PageHelper from '../helper/PageHelper';
import {GalleryInterface} from '../interfaces/gallery/gallery.interface';
import {UserInterface} from '../interfaces/user/user.interface';
import {GalleryItemInterface} from '../interfaces/gallery/gallery-item.interface';
import {flatMap, map, mergeMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServerCommunicationService {

  constructor(@Inject(ENDPOINTS_CONFIG_TOKEN) private endpointsConfig: Endpoints, private http: HttpClient) {
  }

  getNews(pageRequest: PageRequest<NewsItemInterface>): Observable<Page<NewsItemInterface>> {
    return this.http.get<Page<NewsItemInterface>>(this.endpointsConfig.newsEndpoint, {params: PageHelper.toHttpParam(pageRequest)});
  }

  getCurrentUserNews(pageRequest: PageRequest<NewsItemInterface>): Observable<Page<NewsItemInterface>> {
    return this.http.get<Page<NewsItemInterface>>(
      this.endpointsConfig.currentUserNewsEndpoint,
      {params: PageHelper.toHttpParam(pageRequest)}
    );
  }

  getCurrentUserGalleries(pageRequest: PageRequest<GalleryInterface>): Observable<Page<GalleryInterface>> {
    return this.http.get<Page<GalleryInterface>>(
      this.endpointsConfig.currentUserGalleriesEndpoint,
      {params: PageHelper.toHttpParam(pageRequest)}
    );
  }

  getUserNews(username: string, pageRequest: PageRequest<NewsItemInterface>): Observable<Page<NewsItemInterface>> {
    return this.http.get<Page<NewsItemInterface>>(
      this.endpointsConfig.userNewsEndpoint(username),
      {params: PageHelper.toHttpParam(pageRequest)}
    );
  }

  getUserGalleries(username: string, pageRequest: PageRequest<GalleryInterface>): Observable<Page<GalleryInterface>> {
    return this.http.get<Page<GalleryInterface>>(
      this.endpointsConfig.userGalleriesEndpoint(username),
      {params: PageHelper.toHttpParam(pageRequest)}
    );
  }

  getUser(username: string): Observable<UserInterface> {
    return this.http.get<UserInterface>(this.endpointsConfig.userEndpoint(username));
  }

  getDetailedNews(id: number): Observable<NewsItemInterface> {
    return this.http.get<NewsItemInterface>(this.endpointsConfig.detailedNewsEndpoint(id));
  }

  addNews(title: string, content: string, visible: boolean): Observable<NewsItemInterface> {
    return this.http.post<NewsItemInterface>(this.endpointsConfig.newsEndpoint, {title, content, visible}, {observe: 'response'}).pipe(
      mergeMap(response => {
        if (response.status === 201) {
          const location = response.headers.get('Location');
          return this.http.get<NewsItemInterface>(location);
        }
      })
    );
  }

  editNews(id: number, title: string, content: string, visible: boolean): Observable<NewsItemInterface> {
    return this.http.put<NewsItemInterface>(this.endpointsConfig.detailedNewsEndpoint(id), {title, content, visible});
  }

  deleteNews(id: number): Observable<void> {
    return this.http.delete<void>(this.endpointsConfig.detailedNewsEndpoint(id));
  }

  getGallery(id: number): Observable<GalleryInterface> {
    return this.http.get<GalleryInterface>(this.endpointsConfig.galleryEndpoint(id));
  }

  getGalleryImages(id: number, pageRequest: PageRequest<GalleryItemInterface>): Observable<Page<GalleryItemInterface>> {
    return this.http
      .get<Page<GalleryItemInterface>>(this.endpointsConfig.galleryImagesEndpoint(id), {params: PageHelper.toHttpParam(pageRequest)});
  }

  getGalleries(pageRequest: PageRequest<GalleryInterface>): Observable<Page<GalleryInterface>> {
    return this.http.get<Page<GalleryInterface>>(this.endpointsConfig.galleriesEndpoint, {params: PageHelper.toHttpParam(pageRequest)});
  }

  getGalleryImage(imageName: string): Observable<string> {
    return this.http
      // load the image as a blob
      .get(this.endpointsConfig.galleryImageEndpoint(imageName), {responseType: 'blob'})
      // create an object url of that blob that we can use in the src attribute
      .pipe(map(e => URL.createObjectURL(e)));
  }

  getGalleryImageThumbnail(imageName: string): Observable<string> {
    return this.http
      // load the image as a blob
      .get(this.endpointsConfig.galleryImageThumbnailEndpoint(imageName), {responseType: 'blob'})
      // create an object url of that blob that we can use in the src attribute
      .pipe(map(e => URL.createObjectURL(e)));
  }

  addGallery(name: string, description: string, privateGallery: boolean): Observable<GalleryInterface> {
    return this.http.post<GalleryInterface>(this.endpointsConfig.galleriesEndpoint, {
      name,
      description,
      privateGallery
    }, {observe: 'response'}).pipe(
      mergeMap(response => {
        if (response.status === 201) {
          const location = response.headers.get('Location');
          return this.http.get<GalleryInterface>(location);
        }
      })
    );
  }

  editGallery(id: number, name: string, description: string, privateGallery: boolean): Observable<GalleryInterface> {
    return this.http.put<GalleryInterface>(this.endpointsConfig.galleryEndpoint(id), {name, description, privateGallery});
  }

  addGalleryItem(galleryId: number, name: string, description: string, encodedFile: string): Observable<void> {
    return this.http.post<void>(this.endpointsConfig.galleryImagesEndpoint(galleryId), {
      name,
      description,
      encodedFile
    });
  }
}


