import {Component, Inject, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {MASONRY_OPTIONS_TOKEN} from '../config/masonry.config';
import {NgxMasonryOptions} from 'ngx-masonry';
import {Endpoints, ENDPOINTS_CONFIG, ENDPOINTS_CONFIG_TOKEN} from '../config/endpoints.config';
import {ServerCommunicationService} from '../services/server-communication.service';
import {AuthorizationService} from '../services/authorization.service';
import {filter} from 'rxjs/operators';
import {LoginEventEnum} from '../enums/login-event.enum';
import {Page} from '../interfaces/paging/page';
import {NewsItemInterface} from '../interfaces/news/news-item.interface';
import {PaginatedDataSource} from '../interfaces/paging/paginated-data-source.class';
import {GalleryItemInterface} from '../interfaces/gallery/gallery-item.interface';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  data: PaginatedDataSource<NewsItemInterface>;

  constructor(
    private route: ActivatedRoute,
    @Inject(MASONRY_OPTIONS_TOKEN) public masonryConfig: NgxMasonryOptions,
    @Inject(ENDPOINTS_CONFIG_TOKEN) public endpointsConfig: Endpoints,
    private serverCommunicationService: ServerCommunicationService
  ) {
  }

  ngOnInit(): void {

    this.data = new PaginatedDataSource<NewsItemInterface>(
      request => this.serverCommunicationService.getNews(request),
      {property: 'createdAt', direction: 'desc'}, 9
    );
  }

}
