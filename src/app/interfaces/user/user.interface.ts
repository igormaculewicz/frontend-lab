export interface UserInterface {
  id: number;
  username: string;
  roles: string[];
  createdAt: string;
}
