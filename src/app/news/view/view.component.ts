import {Component, OnInit} from '@angular/core';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {ActivatedRoute} from '@angular/router';
import {NewsItemInterface} from '../../interfaces/news/news-item.interface';
import {AuthorizationService} from '../../services/authorization.service';

@Component({
  selector: 'app-news-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class NewsViewComponent implements OnInit {

  newsItem: NewsItemInterface;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    public authorizationService: AuthorizationService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params.id;
      this.serverCommunicationService.getDetailedNews(id).subscribe(response => {
        this.newsItem = response;
      });
    });
  }

}
