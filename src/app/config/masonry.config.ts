import {NgxMasonryOptions} from 'ngx-masonry';
import {InjectionToken} from '@angular/core';

export const MASONRY_OPTIONS_TOKEN = new InjectionToken<NgxMasonryOptions>('masonry-options-token');

export const MASONRY_OPTIONS: NgxMasonryOptions = {
  transitionDuration: '0.8s',
  fitWidth: true,
  gutter: 10
};
