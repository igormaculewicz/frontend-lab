import {Component, ComponentRef, Input, OnInit} from '@angular/core';
import {ServerCommunicationService} from '../../../services/server-communication.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {NgxMasonryComponent} from 'ngx-masonry';

@Component({
  selector: 'app-secured-resource',
  templateUrl: './gallery-item.component.html',
  styleUrls: ['./gallery-item.component.scss']
})
export class GalleryItemComponent implements OnInit {

  @Input() imagePath: string;
  @Input() description: string;
  @Input() thumbnailOnly = false;

  imageLink: SafeResourceUrl;
  thumbnailImageLink: SafeResourceUrl;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private sanitizer: DomSanitizer
  ) {
    if (!this.imagePath) {
      this.thumbnailImageLink = null;
      this.imageLink = null;
    }
  }

  ngOnInit(): void {
    this.serverCommunicationService.getGalleryImageThumbnail(this.imagePath).subscribe(response => {
      this.thumbnailImageLink = this.getTrustedUrl(response);
    });

    if (!this.thumbnailImageLink) {
      this.serverCommunicationService.getGalleryImage(this.imagePath).subscribe(response => {
        this.imageLink = this.getTrustedUrl(response);
      });
    }
  }

  getTrustedUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
