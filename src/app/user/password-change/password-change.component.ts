import {Component, Inject, OnInit} from '@angular/core';
import {AuthorizationService} from '../../services/authorization.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN} from '../../config/endpoints.config';
import {AUTHORIZATION_CONSTANTS_TOKEN, AuthorizationConstants} from '../../constants/authorization.constant';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnInit {

  changePasswordForm: FormGroup;
  passwordChangedSuccessfully: boolean;

  constructor(
    private authorizationService: AuthorizationService,
    private formBuilder: FormBuilder,
    @Inject(ENDPOINTS_CONFIG_TOKEN) private endpointsConfig: Endpoints,
    private http: HttpClient,
  ) {
    this.changePasswordForm = this.formBuilder.group({
      password: '',
      repeatPassword: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(formValues: { password, repeatPassword }) {

    if (!formValues.password) {
      this.changePasswordForm.setErrors(['Password cannot be empty!']);
      return;
    }

    if (!formValues.repeatPassword) {
      this.changePasswordForm.setErrors(['Repeat password cannot be empty!']);
      return;
    }

    if (formValues.password !== formValues.repeatPassword) {
      this.changePasswordForm.setErrors(['Passwords are not equal!']);
      return;
    }

    this.http.put(this.endpointsConfig.changePasswordEndpoint, {password: formValues.password}).subscribe(response => {
      this.passwordChangedSuccessfully = true;
    }, error => {
      console.log(error);
      this.changePasswordForm.setErrors(['Cannot change password!']);
    });
  }
}
