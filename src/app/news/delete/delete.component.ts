import {Component, OnInit} from '@angular/core';
import {NewsItemInterface} from '../../interfaces/news/news-item.interface';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthorizationService} from '../../services/authorization.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class NewsDeleteComponent implements OnInit {

  newsItem: NewsItemInterface;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params.id;

      this.serverCommunicationService.getDetailedNews(id).subscribe(response => {

        if (!this.authorizationService.isLoggedUser(response.creator.username)) {
          this.router.navigate(['/news/' + id]);
        }

        this.newsItem = response;
      }, error => {
        this.location.back();
      });
    });
  }

  performDelete() {
    this.serverCommunicationService.deleteNews(this.newsItem.id).subscribe(() => {
      this.location.back();
    }, error => {
      this.location.back();
    });
  }
}
