import {Component, OnInit} from '@angular/core';
import {PaginatedDataSource} from '../../interfaces/paging/paginated-data-source.class';
import {NewsItemInterface} from '../../interfaces/news/news-item.interface';
import {GalleryInterface} from '../../interfaces/gallery/gallery.interface';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {UserInterface} from '../../interfaces/user/user.interface';
import {AuthorizationService} from '../../services/authorization.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  // TODO should be divided to independent componnets
  newsDisplayedColumns = ['title', 'content', 'createdAt'];
  galleriesDisplayedColumns = ['name', 'createdAt'];

  newsData: PaginatedDataSource<NewsItemInterface>;
  galleriesData: PaginatedDataSource<GalleryInterface>;
  user: UserInterface;
  noUserFoundUsername: string;


  constructor(
    private authorizationService: AuthorizationService,
    private serverCommunicationService: ServerCommunicationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {

      if (this.authorizationService.isLoggedUser(params.username)) {
        this.router.navigate(['user/management']);
      }

      this.serverCommunicationService.getUser(params.username).subscribe(user => {
        this.user = user;

        this.newsData = new PaginatedDataSource<NewsItemInterface>(
          request => this.serverCommunicationService.getUserNews(user.username, request),
          {property: 'title', direction: 'desc'}, 5
        );

        this.galleriesData = new PaginatedDataSource<GalleryInterface>(
          request => this.serverCommunicationService.getUserGalleries(user.username, request),
          {property: 'name', direction: 'desc'}, 5
        );
      }, error => {
        this.noUserFoundUsername = params.username;
      });
    });


  }

}
