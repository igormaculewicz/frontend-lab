import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {LoginEventEnum} from '../enums/login-event.enum';
import {AuthorizationService} from '../services/authorization.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  formMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private authorizationService: AuthorizationService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(loginForm: { username, password }) {
    this.authorizationService.authorize(loginForm.username, loginForm.password).subscribe(response => {
      this.router.navigate(['user/management']);
    }, error => {
      this.formMessage = 'Unauthorized!';
    });

    return false;
  }
}
