import {Inject, Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse, HttpResponse, HttpClient
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {AuthorizationService} from './services/authorization.service';
import {Router} from '@angular/router';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN, UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN} from './config/endpoints.config';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    @Inject(UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN) private unauthorizedEndpoints: string[],
    @Inject(ENDPOINTS_CONFIG_TOKEN) private endpoints: Endpoints,
    private authorizationService: AuthorizationService,
    private http: HttpClient
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({withCredentials: true});

    if (!this.unauthorizedEndpoints.includes(request.url)) {
      const token = this.authorizationService.getToken();
      if (token) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          }
        });
      }
    }

    return next.handle(request).pipe(
      tap(
        event => this.handleResponse(request, event),
        error => this.handleError(request, error)
      )
    );
  }

  handleResponse(req: HttpRequest<any>, event) {
    if (event instanceof HttpResponse) {
      console.log(event);
    }
  }

  handleError(req: HttpRequest<any>, errorEvent) {
    console.log(req);
    if (errorEvent instanceof HttpErrorResponse) {
      if (req.url !== this.endpoints.authorizationEndpoint && errorEvent.status === 401) {
        this.authorizationService.logout();
        this.router.navigate(['login']);
      }
    }
  }
}
