import {Observable} from 'rxjs';

export interface Order<T> {
  property: keyof T;
  direction: 'asc' | 'desc';
}

export interface Sort<T> {
  orders: Order<T>[]
}

export interface PageRequest<T> {
  page: number;
  size: number;
  sort?: Order<T>;
}

export interface Pageable<T> {
  size: number;
  page: number;
  sort: Sort<T>;
}

export interface Page<T> {
  content: T[];
  pageable: Pageable<T>;
  total: number;
}

export type PaginatedEndpoint<T> = (req: PageRequest<T>) => Observable<Page<T>>
