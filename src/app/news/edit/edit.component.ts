import {Component, OnInit} from '@angular/core';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {AuthorizationService} from '../../services/authorization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NewsItemInterface} from '../../interfaces/news/news-item.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class NewsEditComponent implements OnInit {

  public Editor = ClassicEditor;
  newsEditForm: FormGroup;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.newsEditForm = this.formBuilder.group({
      newsId: 0,
      title: '',
      content: '',
      isVisible: false
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params.id;
      if (id !== null) {
        this.serverCommunicationService.getDetailedNews(id).subscribe(response => {

          if (!this.authorizationService.isLoggedUser(response.creator.username)) {
            this.router.navigate(['/news/' + id]);
          }

          this.newsEditForm.setValue({
            newsId: id,
            title: response.title,
            content: response.content,
            isVisible: response.visible
          });
        });
      }
    });
  }

  onSubmit(newsEditForm: { newsId, title, content, isVisible }) {
    if (newsEditForm.newsId === 0) {
      this.serverCommunicationService.addNews(newsEditForm.title, newsEditForm.content, newsEditForm.isVisible)
        .subscribe(response => {
          this.router.navigate(['/news/' + response.id]);
        }, error => {
          console.log(error);
        });
    } else {
      this.serverCommunicationService.editNews(newsEditForm.newsId, newsEditForm.title, newsEditForm.content, newsEditForm.isVisible)
        .subscribe(response => {
          this.router.navigate(['/news/' + response.id]);
        }, error => {
          console.log(error);
        });
    }
    return false;
  }

}
