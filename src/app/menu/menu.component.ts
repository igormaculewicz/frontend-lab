import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from '../services/authorization.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(public authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }

}
