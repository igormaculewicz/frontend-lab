import {environment} from '../../environments/environment';
import {InjectionToken} from '@angular/core';

export const AUTHORIZATION_CONSTANTS_TOKEN = new InjectionToken('authorization-constants');

export interface AuthorizationConstants {
  accessToken: string,
  accessTokenInfo: string
}

export const AUTHORIZATION_CONSTANTS: AuthorizationConstants = {
  accessToken: 'access-token',
  accessTokenInfo: 'access-token-info',
};
