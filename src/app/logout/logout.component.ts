import {Component, OnInit} from '@angular/core';
import {AuthorizationService} from '../services/authorization.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private authorizationService: AuthorizationService) {
    this.authorizationService.logout();
  }

  ngOnInit(): void {
  }

}
