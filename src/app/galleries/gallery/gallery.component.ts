import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MASONRY_OPTIONS_TOKEN} from '../../config/masonry.config';
import {NgxMasonryOptions} from 'ngx-masonry';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN} from '../../config/endpoints.config';
import {ServerCommunicationService} from '../../services/server-communication.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {GalleryInterface} from '../../interfaces/gallery/gallery.interface';
import {PaginatedDataSource} from '../../interfaces/paging/paginated-data-source.class';
import {GalleryItemInterface} from '../../interfaces/gallery/gallery-item.interface';
import {flatMap} from 'rxjs/operators';
import {Observable} from 'rxjs';

declare var jQuery: any;

@Component({
  selector: 'app-galleries-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  gallery: GalleryInterface;
  galleryImages: PaginatedDataSource<GalleryItemInterface>;

  constructor(
    private route: ActivatedRoute,
    @Inject(MASONRY_OPTIONS_TOKEN) public masonryConfig: NgxMasonryOptions,
    @Inject(ENDPOINTS_CONFIG_TOKEN) public endpointsConfig: Endpoints,
    public serverCommunicationService: ServerCommunicationService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serverCommunicationService.getGallery(params.id).subscribe(response => {
        this.gallery = response;
      });

      this.galleryImages = new PaginatedDataSource<GalleryItemInterface>(
        request => this.serverCommunicationService.getGalleryImages(params.id, request),
        {property: 'name', direction: 'desc'}, 9
      );
    });
  }
}
