import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ServerCommunicationService} from '../../../services/server-communication.service';
import {AuthorizationService} from '../../../services/authorization.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './gallery-add.component.html',
  styleUrls: ['./gallery-add.component.scss']
})
export class GalleryAddComponent implements OnInit {

  galleryEditForm: FormGroup;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.galleryEditForm = this.formBuilder.group({
      galleryId: 0,
      name: '',
      description: '',
      privateGallery: false
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params.id;
      if (id !== null) {
        this.serverCommunicationService.getGallery(id).subscribe(response => {

          if (!this.authorizationService.isLoggedUser(response.creator.username)) {
            this.router.navigate(['/gallery/' + id]);
          }

          this.galleryEditForm.setValue({
            galleryId: id,
            name: response.name,
            description: response.description,
            privateGallery: response.privateGallery
          });
        });
      }
    });
  }

  onSubmit(galleryEditForm: { galleryId, name, description, privateGallery }) {
    if (galleryEditForm.galleryId === 0) {
      this.serverCommunicationService.addGallery(galleryEditForm.name, galleryEditForm.description, galleryEditForm.privateGallery)
        .subscribe(response => {
          this.router.navigate(['/gallery/' + response.id + '/details']);
        }, error => {
          console.log(error);
        });
    } else {
      this.serverCommunicationService
        .editGallery(galleryEditForm.galleryId, galleryEditForm.name, galleryEditForm.description, galleryEditForm.privateGallery)
        .subscribe(response => {
          this.router.navigate(['/gallery/' + response.id + '/details']);
        }, error => {
          console.log(error);
        });
    }
    return false;
  }


}
