import {Injectable} from '@angular/core';
import {filter} from 'rxjs/operators';
import {SessionManagementService} from './session-management.service';
import {NavigationEnd, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GlobalActionsService {

  constructor(private sessionManagementService: SessionManagementService, private router: Router) {
  }

  onInit() {
    this.router.events
      .pipe(filter(value => value instanceof NavigationEnd))
      .subscribe(event => {
        this.sessionManagementService.scheduleLogout().subscribe();
      });
  }

  afterViewInit() {
  }
}
