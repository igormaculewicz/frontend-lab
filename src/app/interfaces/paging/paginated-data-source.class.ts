import {map, pluck, share, startWith, switchMap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Order, Page, Pageable, PaginatedEndpoint, Sort} from './page';
import {SimpleDataSource} from './simple-data-source.interface';

export class PaginatedDataSource<T> implements SimpleDataSource<T> {
  private pageNumber = new Subject<number>();
  private sort = new Subject<Order<T>>();

  public page$: Observable<Page<T>>;
  public currentPage: Pageable<T>;
  public total: number;

  constructor(
    endpoint: PaginatedEndpoint<T>,
    initialSort: Order<T>,
    size = 20) {
    this.page$ = this.sort.pipe(
      startWith(initialSort),
      switchMap(sort => this.pageNumber.pipe(
        startWith(0),
        switchMap(page => endpoint({page, sort, size})),
        map(response => {
          this.currentPage = response.pageable;
          this.total = response.total;

          return response;
        })
      )),
      share()
    );
  }

  sortBy(sort: Order<T>): void {
    this.sort.next(sort);
  }

  fetch(page: number): void {
    this.pageNumber.next(page);
  }

  connect(): Observable<T[]> {
    return this.page$.pipe(pluck('content'));
  }

  disconnect(): void {
  }
}
