import {environment} from '../../environments/environment';
import {InjectionToken} from '@angular/core';
import {Observable} from 'rxjs';
import {Page, PageRequest} from '../interfaces/paging/page';

export const ENDPOINTS_CONFIG_TOKEN = new InjectionToken('endpoints-config-token');
export const UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN = new InjectionToken('unauthorized-endpoints-config-token');

export interface Endpoints {
  serverBaseUrl: string
  newsEndpoint: string
  galleriesEndpoint: string
  authorizationEndpoint: string
  changePasswordEndpoint: string
  currentUserNewsEndpoint: string
  currentUserGalleriesEndpoint: string
  userEndpoint: SingleVariableEndpoint<string>
  userNewsEndpoint: SingleVariableEndpoint<string>
  userGalleriesEndpoint: SingleVariableEndpoint<string>
  detailedNewsEndpoint: SingleVariableEndpoint<number>
  galleryImageThumbnailEndpoint: SingleVariableEndpoint<string>
  galleryImageEndpoint: SingleVariableEndpoint<string>
  galleryImagesEndpoint: SingleVariableEndpoint<number>
  galleryEndpoint: SingleVariableEndpoint<number>
}

export const ENDPOINTS_CONFIG: Endpoints = {
  serverBaseUrl: environment.serverBaseUrl,
  newsEndpoint: environment.serverBaseUrl + '/news',
  galleriesEndpoint: environment.serverBaseUrl + '/gallery',
  authorizationEndpoint: environment.serverBaseUrl + '/authorization',
  changePasswordEndpoint: environment.serverBaseUrl + '/user/change-password',
  currentUserNewsEndpoint: environment.serverBaseUrl + '/user/news',
  currentUserGalleriesEndpoint: environment.serverBaseUrl + '/user/gallery',
  userEndpoint: variable => {
    return environment.serverBaseUrl + `/user/${variable}/info`;
  },
  userGalleriesEndpoint: variable => {
    return environment.serverBaseUrl + `/user/${variable}/gallery`;
  },
  userNewsEndpoint: variable => {
    return environment.serverBaseUrl + `/user/${variable}/news`;
  },
  detailedNewsEndpoint: variable => {
    return environment.serverBaseUrl + `/news/${variable}`;
  },
  galleryImageThumbnailEndpoint: variable => {
    return environment.serverBaseUrl + `/gallery/image/${variable}/thumbnail`;
  },
  galleryImageEndpoint: variable => {
    return environment.serverBaseUrl + `/gallery/image/${variable}`;
  },
  galleryImagesEndpoint: variable => {
    return environment.serverBaseUrl + `/gallery/${variable}/image`;
  },
  galleryEndpoint: variable => {
    return environment.serverBaseUrl + `/gallery/${variable}`;
  }
};

export const UNAUTHORIZED_ENDPOINTS: string[] = [
  ENDPOINTS_CONFIG.authorizationEndpoint,
];

export type SingleVariableEndpoint<T> = (variable: T) => string
