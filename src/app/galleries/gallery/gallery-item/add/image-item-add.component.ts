import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ServerCommunicationService} from '../../../../services/server-communication.service';
import {AuthorizationService} from '../../../../services/authorization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GalleryInterface} from '../../../../interfaces/gallery/gallery.interface';
import {Location} from '@angular/common';

@Component({
  selector: 'app-add',
  templateUrl: './image-item-add.component.html',
  styleUrls: ['./image-item-add.component.scss']
})
export class ImageItemAddComponent implements OnInit {
  itemAddForm: FormGroup;
  encodedFile: string;
  imageError: string;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location
  ) {
  }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      const id = params.id;
      const imageId = params.imageId;

      if (id !== null) {
        this.serverCommunicationService.getGallery(id).subscribe(response => {
          if (imageId === undefined) {

            this.itemAddForm = this.formBuilder.group({
              galleryItemId: imageId,
              galleryId: response.id,
              name: '',
              description: ''
            });

          } else {
            // TODO edit
          }
        });
      }
    });
  }

  onSubmit(galleryItemAddForm: { galleryItemId, galleryId, name, description }) {

    this.serverCommunicationService.addGalleryItem(
      galleryItemAddForm.galleryId,
      galleryItemAddForm.name,
      galleryItemAddForm.description,
      this.encodedFile
    ).subscribe(() => {
      this.location.back();
    });

    return false;
  }

  setFile(fileInput: any) {

    if (fileInput.target.files && fileInput.target.files[0]) {
      const maxSize = 20971520;
      const allowedTypes = ['image/jpeg'];

      if (fileInput.target.files[0].size > maxSize) {
        this.imageError =
          'Maximum size allowed is ' + maxSize / 1000 + 'Mb';

        return false;
      }

      if (!allowedTypes.includes(fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG | PNG )';
        return false;
      }

      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.encodedFile = btoa(e.target.result);
      };

      reader.readAsBinaryString(fileInput.target.files[0]);
    }
  }
}
