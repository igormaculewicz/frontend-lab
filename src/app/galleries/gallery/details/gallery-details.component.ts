import {Component, OnInit} from '@angular/core';
import {ServerCommunicationService} from '../../../services/server-communication.service';
import {AuthorizationService} from '../../../services/authorization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {GalleryInterface} from '../../../interfaces/gallery/gallery.interface';
import {PaginatedDataSource} from '../../../interfaces/paging/paginated-data-source.class';
import {GalleryItemInterface} from '../../../interfaces/gallery/gallery-item.interface';

@Component({
  selector: 'app-details',
  templateUrl: './gallery-details.component.html',
  styleUrls: ['./gallery-details.component.scss']
})
export class GalleryDetailsComponent implements OnInit {

  galleriesItemsDisplayedColumns = ['thumbnail', 'name', 'description', 'options'];

  gallery: GalleryInterface;
  galleryItemsData: PaginatedDataSource<GalleryItemInterface>;

  constructor(
    private serverCommunicationService: ServerCommunicationService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params.id;
      this.serverCommunicationService.getGallery(id).subscribe(response => {
        this.gallery = response;

        if (!this.authorizationService.isLoggedUser(response.creator.username)) {
          this.router.navigate(['/gallery/' + id]);
        }

        this.galleryItemsData = new PaginatedDataSource<GalleryItemInterface>(
          request => this.serverCommunicationService.getGalleryImages(params.id, request),
          {property: 'name', direction: 'desc'}, 5
        );
      });
    });
  }
}
