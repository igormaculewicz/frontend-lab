import {PageRequest} from '../interfaces/paging/page';
import {HttpParams} from '@angular/common/http';

export default class PageHelper {
  static toHttpParam(pageRequest: PageRequest<any>): HttpParams {
    return new HttpParams()
      .set('size', pageRequest.size.toString())
      .set('page', pageRequest.page.toString())
      .set('sort', pageRequest.sort.property.toString() + ',' + pageRequest.sort.direction.toString());
  }
}
