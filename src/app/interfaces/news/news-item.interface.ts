import {CreatorInterface} from '../user/creator.interface';

export interface NewsItemInterface {
  id: number;
  title: string;
  content: string;
  visible: boolean;
  creator: CreatorInterface;
  createdAt: string;
}
