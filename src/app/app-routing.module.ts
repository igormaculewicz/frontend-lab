import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GalleriesComponent} from './galleries/galleries.component';
import {NewsComponent} from './news/news.component';
import {LoginComponent} from './login/login.component';
import {SessionExpiredComponent} from './session-expired/session-expired.component';
import {UserManagementComponent} from './user/user-management/user-management.component';
import {LogoutComponent} from './logout/logout.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {UserSearchComponent} from './user/user-search/user-search.component';
import {NewsViewComponent} from './news/view/view.component';
import {NewsEditComponent} from './news/edit/edit.component';
import {NewsDeleteComponent} from './news/delete/delete.component';
import {GalleryComponent} from './galleries/gallery/gallery.component';
import {GalleryAddComponent} from './galleries/gallery/add/gallery-add.component';
import {GalleryDetailsComponent} from './galleries/gallery/details/gallery-details.component';
import {ImageItemAddComponent} from './galleries/gallery/gallery-item/add/image-item-add.component';

const routes: Routes = [
  {path: '', redirectTo: 'news', pathMatch: 'full'},
  {path: 'gallery', component: GalleriesComponent},
  {path: 'news', component: NewsComponent},
  {path: 'news/add', component: NewsEditComponent},
  {path: 'news/:id', component: NewsViewComponent},
  {path: 'news/:id/edit', component: NewsEditComponent},
  {path: 'news/:id/delete', component: NewsDeleteComponent},
  {path: 'login', component: LoginComponent},
  {path: 'session-expired', component: SessionExpiredComponent},
  {path: 'gallery/add', component: GalleryAddComponent},
  {path: 'gallery/:id', component: GalleryComponent},
  {path: 'gallery/:id/edit', component: GalleryAddComponent},
  {path: 'gallery/:id/details', component: GalleryDetailsComponent},
  {path: 'gallery/:id/image-add', component: ImageItemAddComponent},
  {path: 'gallery/:id/image-edit/:imageId', component: ImageItemAddComponent},
  {path: 'user/management', component: UserManagementComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'user/search', component: UserSearchComponent},
  {path: 'user/:username', component: UserProfileComponent},
  {path: '**', redirectTo: 'news'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
