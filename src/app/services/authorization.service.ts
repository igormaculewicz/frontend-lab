import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN} from '../config/endpoints.config';
import {TokenInterface} from '../interfaces/authorization/token.interface';
import * as jwt_decode from 'jwt-decode';
import {AUTHORIZATION_CONSTANTS_TOKEN, AuthorizationConstants} from '../constants/authorization.constant';
import {TokenInfoInterface} from '../interfaces/authorization/token-info.interface';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {


  constructor(
    @Inject(ENDPOINTS_CONFIG_TOKEN) private endpointsConfig: Endpoints,
    @Inject(AUTHORIZATION_CONSTANTS_TOKEN) private authorizationConstants: AuthorizationConstants,
    private http: HttpClient
  ) {
  }

  return;

  authorize(username: string, password: string): Observable<TokenInterface> {
    return this.http.post<TokenInterface>(this.endpointsConfig.authorizationEndpoint, {
      username,
      password
    }).pipe(map(response => {
        localStorage.setItem(this.authorizationConstants.accessToken, response.token);

        const decodedToken: TokenInfoInterface = jwt_decode(response.token);

        localStorage.setItem(this.authorizationConstants.accessTokenInfo, JSON.stringify(decodedToken));
        console.log('Logged as: %s', decodedToken.aud);

        return response;
      }
    ));
  }

  logout() {
    localStorage.removeItem(this.authorizationConstants.accessToken);
    localStorage.removeItem(this.authorizationConstants.accessTokenInfo);
  }

  getToken(): string {
    return localStorage.getItem(this.authorizationConstants.accessToken);
  }

  getTokenInfo(): TokenInfoInterface {
    const item = localStorage.getItem(this.authorizationConstants.accessTokenInfo);

    return JSON.parse(item);
  }

  getTokenExpirationSeconds(): number {
    const tokenInfo = this.getTokenInfo();
    const tokenSeconds = tokenInfo?.exp - (Math.floor(Date.now() / 1000));

    return tokenSeconds > 0 ? tokenSeconds : 0;
  }

  isLogged(): boolean {
    return this.getTokenExpirationSeconds() > 0;
  }

  isLoggedUser(username: string): boolean {
    return this.getTokenInfo()?.aud === username;
  }

}
