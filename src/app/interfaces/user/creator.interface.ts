export interface CreatorInterface {
  id: number;
  username: string;
}
