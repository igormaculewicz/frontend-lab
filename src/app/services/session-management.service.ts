import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthorizationService} from './authorization.service';

@Injectable({
  providedIn: 'root'
})
export class SessionManagementService {

  constructor(private authorizationService: AuthorizationService,) {
  }

  scheduleLogout(): Observable<void> {
    return new Observable<void>(next => {
      if (this.authorizationService.getToken() !== null) {
        const expirationTimeMs = this.authorizationService.getTokenExpirationSeconds() * 1000;
        console.log('Scheduling logout to: %d ms', expirationTimeMs);
        setTimeout(() => {
          console.log('Logged out!');
          this.authorizationService.logout();
          next.next();
        }, expirationTimeMs);
      }
    });
  }
}
