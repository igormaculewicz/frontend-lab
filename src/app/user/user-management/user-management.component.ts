import {Component, Inject, OnInit} from '@angular/core';
import {Endpoints, ENDPOINTS_CONFIG_TOKEN, UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN} from '../../config/endpoints.config';
import {AuthorizationService} from '../../services/authorization.service';
import {TokenInfoInterface} from '../../interfaces/authorization/token-info.interface';
import {CountdownConfig} from 'ngx-countdown';
import {Router} from '@angular/router';
import {SessionManagementService} from '../../services/session-management.service';
import {MatTabChangeEvent} from '@angular/material/tabs';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

  countdownConfig: CountdownConfig = {};
  tokenInfo: TokenInfoInterface;

  constructor(
    @Inject(UNAUTHORIZED_ENDPOINTS_CONFIG_TOKEN) private unauthorizedEndpoints: string[],
    @Inject(ENDPOINTS_CONFIG_TOKEN) private endpoints: Endpoints,
    private authorizationService: AuthorizationService,
    private router: Router,
    private sessionManagementService: SessionManagementService
  ) {
    this.sessionManagementService.scheduleLogout().subscribe(() => {
      this.router.navigate(['logout']);
    });

    this.tokenInfo = this.authorizationService.getTokenInfo();

    if (this.tokenInfo === null) {
      this.authorizationService.logout();
      this.router.navigate(['login']);
    }
  }

  ngOnInit(): void {
    this.countdownConfig.leftTime = this.authorizationService.getTokenExpirationSeconds();
  }
}
